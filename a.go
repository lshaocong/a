package a

import (
	"fmt"
)

func Sum(lhs int, rhs int) int{
	return lhs+rhs;
}

func Greet(name string) {
	fmt.Println("hello",name)
}
